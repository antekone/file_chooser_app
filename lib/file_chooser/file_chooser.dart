import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'file_descriptor.dart';
import '../list_view/list_view_widget.dart';

import 'dart:io';

enum FileChooserDisplayType {
  Details, Icons, Tiles
}

class Filter {
  const Filter({this.pattern, this.patternCaption});

  final String pattern;
  final String patternCaption;
}

class FileChooserConfig {
  const FileChooserConfig({
    // Default configuration of this file chooser.
    this.initialDir = "/",
    this.displayType = FileChooserDisplayType.Tiles,
    this.caption = "File selection",
    this.filters = const [
      Filter(pattern: "*.*", patternCaption: "All files")
    ],
  });

  final String initialDir;
  final String caption;
  final List<Filter> filters;
  final FileChooserDisplayType displayType;
}

class FileChooserWidget extends StatefulWidget {
  @override
  _FileChooserWidgetState createState() => _FileChooserWidgetState();
}

class _FileChooserWidgetState extends State<FileChooserWidget> {
  @override
  Widget build(BuildContext context) {
    var columns = <ColumnInfo> [
      ColumnInfo(sizable: false, width: 18.0),
      ColumnInfo(title: "Name", expanding: true),
      ColumnInfo(title: "Size", width: 64.0),
      ColumnInfo(title: "Date", sizable: false, width: 82.0)
    ];

    List<ColumnRow> _enumFilesToRow() {
    }

    Future<List<ColumnRow>> _listFiles() async {
      var systemRootDir = Directory.fromUri(Uri.directory("/etc"));

      var fsTable = List<FileDescriptor>();
      for(var entity in systemRootDir
          .listSync(recursive: false, followLinks: false)) {
        debugPrint("entity: $entity");

        var fd = FileDescriptor();
        var statResult = entity.statSync();

        fd.setName(entity.path);
        fd.setSize(statResult.size);
        switch(statResult.type) {
          case FileSystemEntityType.file:
            fd.setType(Type.File);
            break;
          case FileSystemEntityType.directory:
            fd.setType(Type.Directory);
            break;
          default:
            fd.setType(Type.Unknown);
            break;
        }

        fsTable.add(fd);
      }

      debugPrint("fstable: $fsTable");

      List<ColumnRow> ret = fsTable.map((var input) {
        return ColumnRow(columns: [
          SizedBox(
              width: 16,
              height: 16,
              child: DecoratedBox(
                  decoration: BoxDecoration(
                      image: DecorationImage(
                          image: AssetImage("assets/blank-file-16.png"))))),

          Text(input.getName(), overflow: TextOverflow.ellipsis),
          Text("${input.getSize()} b", overflow: TextOverflow.ellipsis),
          Text("Date", overflow: TextOverflow.ellipsis)
        ]);
      }).toList(growable: true);

      debugPrint("$ret");

      return Future.value(ret);
    }

    return Scaffold(
      body: SafeArea(child: ListViewWidget(initialColumns: columns, dataProvider: _listFiles))
    );
  }
}
