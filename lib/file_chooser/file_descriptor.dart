enum Type {
  File, Directory, Link, Unknown
}

class FileDescriptor {
  String name;
  int size;
  Type type;

  FileDescriptor();

  void setName(String n) =>
      this.name = n;

  void setSize(int s) =>
      this.size = s;

  void setType(Type t) =>
      this.type = t;

  String getName() =>
      this.name;

  int getSize() =>
      this.size;

  Type getType() =>
      this.type;
}