import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

class ColumnInfo {
  ColumnInfo({this.title = "", this.sizable = true, this.expanding = false, this.width = 32.0});

  final String title;
  final bool sizable;
  final bool expanding;
  final double width;
  bool last;
}

class ColumnRow {
  final List<Widget> columns;
  final bool empty;

  ColumnRow({@required this.columns, this.empty});
}

typedef DataProviderFunction = Future<List<ColumnRow>> Function();

class ListViewWidget extends StatefulWidget {
  final List<ColumnInfo> initialColumns;
  final DataProviderFunction dataProvider;

  ListViewWidget({
    @required this.initialColumns,
    @required this.dataProvider
  });

  @override
  State<StatefulWidget> createState() {
    return _ListViewWidgetState(
        columns: this.initialColumns,
        dataProvider: this.dataProvider);
  }
}

class _ListViewWidgetState extends State<ListViewWidget> {
  final double spacerSize = 8;
  var columns = List<ColumnInfo>();
  var widths = List<double>();
  double lastScreenWidth = 0;
  bool dataReady = false;
  DataProviderFunction dataProvider;
  List<ColumnRow> data;

  _ListViewWidgetState({@required List<ColumnInfo> columns, @required this.dataProvider}) {
    setColumns(columns);
  }

  @override
  void initState() {
    super.initState();

    this.dataProvider().then((List<ColumnRow> d) {
      setState(() {
        this.data = d;
        this.dataReady = true;
      });
    });
  }

  void addColumn(ColumnInfo ci) {
    columns.add(ci);
    widths.add(-1);
  }

  void setColumns(List<ColumnInfo> lst) {
    this.columns = lst;
    this.widths.clear();
    for(var _ in this.columns) {
      widths.add(-1);
    }
  }

  Widget _buildListViewItem(BuildContext ctx, int index) {
    if(!this.dataReady)
      return null;

    if(index >= this.data.length)
      return null;

    var fontHeight = Theme.of(ctx).textTheme.body1.height;
    var childRow = List<Widget>();
    var row = this.data[index];
    var sizeSizer = SizedBox(width: this.spacerSize, child: Center(child: Text("")));

    for(int ri = 0, rlen = row.columns.length; ri < rlen; ri++) {
      childRow.add(SizedBox(height: fontHeight, width: this.widths[ri], child: row.columns[ri]));

      if(ri != rlen - 1) {
        childRow.add(sizeSizer);
      }
    }

    return Row(children: childRow);
  }

  Widget _buildListViewItems(BuildContext ctx) {
    return this.dataReady ? ListView.builder(
        itemExtent: 20.0,
        itemCount: this.data.length,
        itemBuilder: _buildListViewItem
    )
    :
    Center(child: CircularProgressIndicator());
  }

  void _reLayout(BuildContext ctx) {
    for(int i = 0, len = columns.length; i < len; i++) {
      columns[i].last = (i == (len - 1));
    }

    int autosizedColumnCount = 0;
    double fixedColumnSizes = 0;
    for(var ci in columns) {
      if(ci.expanding) {
        autosizedColumnCount++;
      } else {
        fixedColumnSizes += ci.width;
      }

      if(!ci.last) {
        fixedColumnSizes += spacerSize;
      }
    }

    double expandedColumnWidth = 0;
    if(autosizedColumnCount > 0) {
      var availableSpace = MediaQuery.of(ctx).size.width - fixedColumnSizes;
      expandedColumnWidth = availableSpace / autosizedColumnCount;
    }

    setState(() {
      for(int idx = 0, len = columns.length; idx < len; idx++) {
        if(columns[idx].expanding) {
          widths[idx] = expandedColumnWidth;
        } else {
          widths[idx] = columns[idx].width;
        }
      }
    });
  }

  bool _needsReLayout() {
    for(int i = 0, len = this.widths.length; i < len; i++) {
      if(this.widths[i] == -1)
        return true;
    }

    return false;
  }

  void _doReLayout(BuildContext ctx, int index, double delta) {
    this.widths[index] += delta;
    this.widths[index + 1] -= delta;
  }

  bool _tryReLayout(BuildContext ctx, int index, double delta) {
    double maxWidth = MediaQuery.of(ctx).size.width;
    for(int i = 0, len = this.widths.length; i < len; i++) {
      double width;

      if(i == index)
        width = this.widths[i] + delta;
      else if(i == index + 1) {
        width = this.widths[i] - delta;
      } else {
        width = this.widths[i];
      }

      if(width < 0)
        return false;

      maxWidth -= width;
    }

    maxWidth -= (this.widths.length - 1) * this.spacerSize;
    if(maxWidth < 0)
      return false;

    return true;
  }

  void _beginDrag(BuildContext ctx, int index) {
  }

  void _updateDrag(BuildContext ctx, int index, double delta) {
    if(this._tryReLayout(ctx, index, delta)) {
      setState(() {
        this._doReLayout(ctx, index, delta);
      });
    }
  }

  void _finishDrag(BuildContext ctx, int index) {
  }

  Widget _buildListViewHeader(BuildContext ctx) {
    final boldStyle = TextStyle(fontWeight: FontWeight.bold);
    List<Widget> columnRow = new List<Widget>();

    for(int i = 0, len = columns.length; i < len; i++) {
      var ci = this.columns[i];
      var widgetWidth = this.widths[i];

      columnRow.add(SizedBox(width: widgetWidth, child: Text(ci.title, style: boldStyle, overflow: TextOverflow.ellipsis)));

      if(i != this.columns.length - 1) {
        if(ci.sizable) {
          var gestureDetector = GestureDetector(
              onHorizontalDragStart: (var _) =>
                  this._beginDrag(ctx, i),

              onHorizontalDragUpdate: (var drag) =>
                  this._updateDrag(ctx, i, drag.delta.dx),

              onHorizontalDragEnd: (var _) =>
                  this._finishDrag(ctx, i),

              child: SizedBox(width: this.spacerSize, child: Text("|"))
          );

          columnRow.add(gestureDetector);
        } else {
          columnRow.add(SizedBox(width: this.spacerSize, child: Text("|")));
        }
      }
    }

    return Row(children: columnRow);
  }

  Widget _buildListView(BuildContext ctx) {
    var headerColumn = Column(
      children: <Widget> [
        this._buildListViewHeader(ctx),
        Expanded(child: this._buildListViewItems(ctx)),
      ],
    );

    return headerColumn;
  }

  AppBar _buildAppBar() {
    return AppBar(title: Text("Padding"));
  }

  @override
  Widget build(BuildContext context) {
    var currentScreenWidth = MediaQuery.of(context).size.width;
    if(this._needsReLayout() || this.lastScreenWidth != currentScreenWidth) {
      this._reLayout(context);
      this.lastScreenWidth = currentScreenWidth;
    }

    return Container(
        child: this._buildListView(context)
    );
  }
}
